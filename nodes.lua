-- Bongos
minetest.register_node("drums:bongo_small", {
	description = "Small Bongo",

	tiles = { 
		"default_tree_top.png^[brighten",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
		"default_wood.png",
	},
	drawtype = "nodebox",
	node_box = {
		type = "fixed",
		fixed = {
			{-0.25, -0.5, -0.25,	0.25, 0.0, 0.25}
		}
	},
	is_ground_content = true,
	groups = {choppy=3},
	drop = "drums:bongo_small",

	on_rightclick = function()
		minetest.sound_play("drums_small-bongo-play", {gain = 1})
	end
})

