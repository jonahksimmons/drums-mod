# Drums
Mod for [Minetest](https://www.minetest.net/) that adds drums to the game.


## Features
- Small Bongo


Right click on the node to play them.


## Goals
- Large Bongo
- Drum machine sounds
- Full drum kit
- Tamborine / various percussion
- Good quality samples
- Allow it to be powered by mesecons


## Compatability
This is built for the base game of Minetest.


## License
[LGPL 3.0 only](https://spdx.org/licenses/LGPL-3.0-only.html) for all code
[CC-by-SA 4.0](https://spdx.org/licenses/CC-BY-SA-4.0.html) for all sound

